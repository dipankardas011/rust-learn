fn main() {
    let tup  = (500, 2.4, 1);
    let (x,y,z) = tup;

    println!("Tuple {x} {y} {z}");
    let first: i32 = tup.0;
    let second: f32 = tup.1;
    let thrid: u8 = tup.2;
    println!("Tuple {first} {second} {thrid}");

    // let ATup: (i32, f64, u8) = ();
    
    let a: [i32; 5] = [1,2,3,5,5];
    let b:[i32;5] = [3;5];
    let first = a[2];
    println!("{first}")
}
