enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

fn get(coin: Coin) -> u8 {
    match coin {
        Coin::Dime => 10,
        Coin::Penny => {
            std::println!("Penny found!");
            1
        }
        Coin::Nickel => 20,
        Coin::Quarter => 25,
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i+1),
    }
}

fn main(){
    std::println!("{}",get(Coin::Penny));
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    std::println!("{}",six.unwrap());
    std::println!("{}",none.is_none());


    let config_max = Some(3u8);
    match config_max {
        Some(max) => std::println!("The maximum is configured to be {}", max),
        _ => (), // it will ignore all none values
    }
}
