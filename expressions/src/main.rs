fn main() {
    let number = 6;
    if number % 4 ==0 {
        println!("{number} is divisible by 4");
    } else if  number %3 ==0 {
        println!("{number} is divisible by 3");
    } else {
        println!("{number} is not divisible by 4 or 3");
    }
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {result}");

    loop_labels();

    loop_while();

    let a = [10,20,30,4];
    for element in a {
        print!("{element} ");
    }
    println!();

    for number in (1..4).rev() {
        println!("{number}");
    }
    
}

fn loop_while() {
    let mut number = 3;
    while number != 0{
        println!("{number}!");
        number -= 1;
    }
    println!("LIFTOFF!!");
}

fn loop_labels() {
    let mut count = 0;
    'counting_up: loop {
        println!("count = {count}");
        let mut remaining = 10;

        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
            remaining -= 1;
        }

        count += 1;
    }
    println!("End count = {count}");
}
