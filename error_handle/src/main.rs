use std::{io::{self, ErrorKind}, fs::File};


fn main(){
    let greeting = File::open("main.c");

    // let greet = match greeting {
    //     Ok(F) => F,
    //     Err(err) => panic!("Problem opening the file: {:?}", err)
    // };
    let greet = match greeting {
        Ok(F) => F,
        Err(err) => match err.kind() {
            ErrorKind::NotFound => match File::create("main.c") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_err => {
                panic!("Problem opening the file {:?}", other_err);
            }
        }
    };

    // using Result<T, E>

    let greeting_file = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });

}
