use std::{fs::File, io::{self, Read}, error::Error};

fn read_username_from_file() -> Result<String, io::Error> {
    let mut username = String::new();

    File::open("Hello.txt")?.read_to_string(&mut username)?;
    Ok(username)
}

fn main() -> Result<(), Box<dyn Error>> {
    let greeting_file = File::open("hello.txt")?;

    Ok(())
}

fn largest<T: std::cmp::PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];
    for item in list {
        if item > largest {
            largest = item;
        }
    }
    largest
}

