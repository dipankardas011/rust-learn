pub mod vm {
    use std::println;

    pub fn create(no: u8) {
        println!("Created the cluster with {} nodes", no);
    }

    pub fn delete() {
        println!("Deleted the cluster with nodes");
    }
}
