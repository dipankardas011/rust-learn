pub mod azure_provider {

    #[derive(Debug)]
    pub struct ClientStruct {
        pub client: u8,
        pub is_ha: bool
    }

    use std::println;

    pub mod vm;

    pub fn init(ha: bool) -> ClientStruct {
        println!("azure provider client created");

        ClientStruct { client: 0x34, is_ha: ha}
    }

    impl ClientStruct {

        pub fn create_ha(&self, no_cp: u8, no_wp: u8) {
            println!("Controlplane");
            for i in 0..no_cp {
                vm::vm::create(i);
            }


            println!("Workerplane");
            for i in 0..no_wp {
                vm::vm::create(i);
            }

            let kubeconfig: &str = r#"
            apiVersion: v1
            name: hello
            spec: {}
            type: ha
            "#;

            println!("Kubeconfig\n{}",kubeconfig);

            println!("CREATED!!");
        }


        pub fn create_managed(&self, nodes: u8) {
            println!("nodes");
            for i in 0..nodes {
                vm::vm::create(i);
            }

            let kubeconfig: &str = r#"
            apiVersion: v1
            name: hello
            spec: {}
            type: managed
            "#;

            println!("Kubeconfig\n{}",kubeconfig);

            println!("CREATED!!");
        }


        pub fn delete_ha(&self) {
            vm::vm::delete();
            println!("DELETED!! (HA)");
        }

        pub fn delete_managed(&self) {
            vm::vm::delete();
            println!("DELETED!! (Managed)");
        }
    }

}


pub mod civo_provider {
    use std::println;

    pub mod vm;

    pub fn init() {
        println!("civo provider client");
    }

    // TODO: Add the needed
}
