use ksctl_api;
use std::{io, println};


fn main() {

    println!("welcome to ksctl remap in Rust[🦀]");

    println!(r#"
Operations:
[1] create
[2] delete
    "#);
    let mut input = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut input).unwrap();
    let operation = input.trim().parse::<i32>().unwrap();

    match operation {
        1 => {
            println!("Enter the whether to create ha or managed?");
            let mut input = String::new();
            let stdin = io::stdin();
            stdin.read_line(&mut input).unwrap();
            let bool_input = input.trim().parse::<bool>().unwrap();

            ksctl_api::create_cluster(bool_input);
        }
        2 => {
            println!("Enter the whether to delete ha or managed?");
            let mut input = String::new();
            let stdin = io::stdin();
            stdin.read_line(&mut input).unwrap();
            let bool_input = input.trim().parse::<bool>().unwrap();

            ksctl_api::delete_cluster(bool_input);
        }
        _ => {
            panic!("Invalid request")
        }
    }

}
