
pub mod ksctl;

use std::println;

pub use crate::ksctl::azure_provider;
pub use crate::ksctl::civo_provider;

pub fn create_cluster(isha: bool) {
    let client: azure_provider::ClientStruct = azure_provider::init(isha);
    println!("{:?}", client);
    if isha {
        client.create_ha(3, 2);
    } else {
        client.create_managed(4);
    }
}


pub fn delete_cluster(isha: bool) {
    let client: azure_provider::ClientStruct = azure_provider::init(isha);
    println!("{:?}", client);
    if isha {
        client.delete_ha();
    } else {
        client.delete_managed();
    }
}
