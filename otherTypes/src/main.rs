use std::collections::HashMap as hsmap;
use rand::Rng;

// use std::io::{self, Write}; // it means to use std::io and its submodule
// use std::{cmp::Ordering, io}; // it is used to combine it in single line

// use std::collections::*;

fn main() {
    let mut map = hsmap::new();
    map.insert(1, 2);
    std::println!("{:?}", map);
    let rand_number = rand::thread_rng().gen_range(1..=100);

    std::println!("{}", rand_number);
}
