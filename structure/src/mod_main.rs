
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"), // here the chief has the access to what
                // the seasonal fruits it should pick based on the season
                // so the customer is left to decide on what must be the toast be made of
            }
        }
    }
}


pub fn eat_at_restaurant() {
    let mut meal = back_of_house::Breakfast::summer("Rye");
    meal.toast = String::from("Wheat");
    std::println!("id like {} toast please", meal.toast);
}

fn main() {
    eat_at_restaurant();
}
