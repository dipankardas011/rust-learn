#[derive(Debug)]
struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn is_width_positive(&self) -> bool {
        self.width > 0
    }

    fn fix_width(&mut self) {
        self.width = 999
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Self {
        Self {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let temp = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("dipa@gmail.com"),
        sign_in_count: 1,
    };
    println!("{}", temp.active);

    println!("{:?}",temp);
    // let black = Color(0, 0, 0);

    let scale = 2;
    let mut rect1 = Rectangle {
        width: dbg!(30*scale),
        height: 50,
    };
    dbg!(&rect1);
    println!("area = {}", rect1.area());
    println!("is width +ve {}", rect1.is_width_positive());
    rect1.fix_width();
    println!("{:?}", rect1);
    let rect2 = Rectangle {
        width: 40*scale,
        height: 60,
    };
    dbg!(&rect2);
    println!("rect1 can hold rect2? {}", rect1.can_hold(&rect2));


    let square = Rectangle::square(32);
    println!("area of square {}", square.area());
}

// struct Color(i32, i32, i32);
// struct Point(i32, i32, i32);
