fn main() {
    let mut s = String::from("Hello");
    let r1 = &s;
    let r2 = &s;
    // s.push_str(", World");
    println!("{s} {r1} {r2}");
    // variables r1 and r2 will not be used after this point

    s.push_str(", World");

    let r3 = &mut s;

    change(r3); // or
    change(&mut s);
    println!("{s}");
}

fn change(str: &mut String) {
    str.push_str(", Dipankar")
}
