fn main() {
    let str: String = String::from("I am Dipankar");
    let str2: String = String::from("abcdsc");

    let mut ret = first_word(&str);
    println!("{str} -> len({ret})");

    ret = first_word(&str2);
    println!("{str2} -> len({ret})");

    // inorder to get the slice of data
    let a = &str[..5];
    println!("{a}");

    let str_literal = "i am";
    // ret = first_word(&str_literal[..]);

    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    assert_eq!(slice, &[2, 3]);
}

// &str -> string slice
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}
