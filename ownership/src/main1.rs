fn main() {
    let s1 = gives_ownership();
    println!("{}", s1);

    let s2 = String::from("hello");

    let s3 = takes_and_gives_back(s2);

    println!("{}", s3);

    let stemp = String::from("hello");
    let len = cal_length_reference(&stemp);
    println!("The length '{}' is {}.", stemp, len);
}

fn gives_ownership() -> String {
    let temp = String::from(", u");
    temp
}

fn takes_and_gives_back(a: String) -> String {
    a
}

#[warn(dead_code)]
fn cal_length(s: String) -> (String, usize) {
    let len = s.len();
    (s, len)
}

fn cal_length_reference(a: &String) -> usize {
    a.len()
}
