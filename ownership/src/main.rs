fn main() {
    // how to create a String from string literal
    let mut old_s = String::from("hello");
    old_s.push_str(", world!"); // push_str() appends a literal to a String
    println!("{}", old_s);

    let mut s = String::new();
    s.push('1');
    s.push_str("My name is Dipanakr");
    println!("{}", s);

    let s1 = String::from("reference_data");
    let s2 = s1;
    println!("{}", s2);
    // println!("{}", s1);

    let s3 = s2.clone();
    println!("s2={} s3={}", s2, s3);
}
