use std::{println, fmt::Display};



struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

fn main() {
    let p = Point{x: 5, y:10};
    println!("p.x = {}", p.x());


    let tweet = Tweet{
        username: String::from("horse_ebooks"),
        content: String::from("nice"),
        reply: false,
        retweet: false,
    };
    println!("1 new tweet: {}", tweet.summarize());
    notify(&tweet)
}




pub trait Summary {
    fn summarize(&self) -> String;
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

// to use the default trait props
// impl Summary for NewsArticle {}
//

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}


// for passing the implementation of trait call
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// for the generic one
pub fn notify_t<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize());
}

// we specify in the notify definition that item must implement both Display and Summary .
// pub fn notify(item: &(impl Summary + Display)) {}
// pub fn notify<T: Summary + Display>(item: &T) {}

// fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {}
//
// fn some_function<T,U>(t: &T, u: &U)->i32
// where
//     T: Display + Clone,
//     U: Clone + Debug,
// {}
