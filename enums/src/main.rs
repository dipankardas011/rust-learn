
#[derive(Debug)]
enum IpAddr {
    V4,
    V6,
}

#[derive(Debug)]
struct Ip {
    kind: IpAddr,
    address: String,
}

// struct Ipv4Addr {
// // --snip--
// }
// struct Ipv6Addr {
// // --snip--
// }
// enum IpAddr {
// V4(Ipv4Addr),
// V6(Ipv6Addr),
// }

fn main() {

    let home = Ip {
        kind: IpAddr::V4,
        address: String::from("127.0.0.1"),
    };

    let loopback = Ip {
        kind: IpAddr::V6,
        address: String::from("::1"),
    };
    dbg!(&home);
    dbg!(&loopback);
    let m = Message::Write(String::from("hello"));
    m.call();
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {

    }
}
