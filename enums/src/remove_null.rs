enum Option<T> {
    None,
    Some(T),
}

fn main() {
    let some_no = Some(5);
    let some_chr = Some('e');

    let absent_number: Option<i32> = None;

    let a = None;
}
