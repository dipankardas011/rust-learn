use std::{vec, println, print};

fn main() {
    let mut v: Vec<i32> = Vec::new();
    v.push(5);
    v.push(9);
    v.push(8);
    println!("{:?}", v);

    let first: &i32 = &v[0];
    println!("first {}", first);

    let first: Option<&i32> = v.get(0);
    match first {
        Some(first) => println!("first element: {}", first),
        None => println!("No first element"),
    }


    // also their is another method which can 
    // create the new vec
    let v1 = vec![1, 2, 3];
    println!("{:?}",v1);
    let mut sum = 0;
    // -------------------
    // let mut iter = &v1;
    // for _ in iter {
    //     iter.push(4);
    // }
    // -------------------
    for i in &v1 {
        print!("{i} ");
        sum += *i;
        // v1.push(2);
        // also you can 
        // *i += 50;
    }
    println!("\nSum: {sum}");


    let mut row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
    println!("{:?}",row);
    println!("{:?}", row.pop());
}

#[derive(Debug)]
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}
