fn first_word(s: &String) -> &str {

    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i]
        }
    }
    return &s[..]
}

fn main() {
    let string = String::from("Hello World!");
    let first_world = first_word(&string);

    println!("{}",first_world);

}
