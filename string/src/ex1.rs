use std::{vec, println, collections::HashMap};

fn calculate_median(mut arr: Vec<i32>) -> f64 {
    let mut median = 0.0;
    arr.sort();

    let N = arr.len();
    if N & 1 == 0 {
        median = (arr[N/2] + arr[(N-1)/2]) as f64 / 2.0 as f64;
    } else {
        median = arr[N/2] as f64;
    }

    median
}

fn calculate_mode(arr: Vec<i32>) -> i32 {
    let mut mode = -1;

    let mut hs = HashMap::new();
    for i in arr {
        let count = hs.entry(i).or_insert(0);
        *count += 1;
    }


    let mut max_count = 0;
    for (key, value) in hs {
        if max_count < value {
            max_count = value;
            mode = key
        }
    }

    mode
}

fn main() {
    let arr = vec![2,3,4,6, 1, 1];
    let arr2 = arr.clone();
    println!("median: {}",calculate_median(arr));
    println!("mode: {}",calculate_mode(arr2));
}
