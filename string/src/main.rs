use std::{println, format, collections::HashMap};

fn main() {
    let mut orgS = String::new();

    let data = "initial contents";
    let s = data.to_string();
    let s1 = "".to_string();

    let s2 = String::from(data);
    let hello = String::from("नमस्ते"); // these are not supoorted to get looped


    orgS.push(' ');
    orgS.push_str("hello");
    println!("{}", orgS);
    println!("{}", hello);


    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
    let kubeconfig = format!("{}, config", "abcd");

    println!("{}", kubeconfig);

    // println!("{}", s2[0]); // indexing on String not supported as Vec<u8> take a look at 188-189 pg

    let iterS = String::from("Зд");
    for c in iterS.chars() {
        println!("{c}");
    }

    for b in iterS.bytes() {
        println!("{b}");
    }

    for c in hello.chars() {
        println!("{c}");
    }

    // for b in hello.bytes() {
    //     println!("{b}");
    // }

    // -----------------------------
    // lets have hasmap
    // -----------------------------
    let mut scores = HashMap::new();
    scores.insert(String::from("blue"), 10);
    scores.insert(String::from("yellow"), 12);


    let team_name = String::from("Blue");
    let score = scores.get(&team_name).copied().unwrap_or(-1);

    println!("{score}");
    println!("{:?}",scores);

    for (key, val) in &scores {
        println!("{key} {val}");
    }
    scores.entry(String::from("Yellow")).or_insert(50); //The or_insert method on Entry is defined to return a mutable reference to the value for
    // the corresponding Entry key if that key exists, and if not, inserts the parameter as the new
    // value for this key and returns a mutable reference to the new value.
    let text = "hello world wonderful world";
    let mut map = HashMap::new();
    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);
}
