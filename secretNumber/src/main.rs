use std::{io, cmp::Ordering};
use rand::Rng;

fn main() {
    println!("Guess the number!");
    let secretNum = rand::thread_rng().gen_range(1..=100);
    loop {

        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Failed to readline");

        // rather to crash the program in panic
        // let g: u32 = guess.trim().parse::<u32>().expect("Please type a number!");

        // we can use other method
        // match is used to check the result with predefined Enums
        let g: u32 = match guess.trim().parse::<u32>() {
            Ok(num) => num,
            Err(_) => continue,
        };


        println!("Your guessed: {g}");

        match g.cmp(&secretNum){
            Ordering::Less => println!("Too Small!"),
            Ordering::Greater => println!("Too Big!!!!"),
            Ordering::Equal => {
                println!("You win!!");
                break;
            },
        }
    }
}
